package net.infobank.itv.mo_datauser.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.model.Schedule;

public class ScheduleDAO {
    private static Logger log = LoggerFactory.getLogger(ScheduleDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public ScheduleDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

	public List<Schedule> selectMoSchedule() {
		List<Schedule> list = new ArrayList<Schedule>();
		SqlSession session = sqlSessionFactory.openSession();

		try {
			list = session.selectList("Schedule.selectMoSchedule");
		} catch(Exception e)  {
		    log.error("Schedule.selectMoSchedule : " , e);
		} finally {
			session.close();
		}

		return list;
	}

}
