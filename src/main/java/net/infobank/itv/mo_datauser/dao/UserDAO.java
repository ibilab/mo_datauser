package net.infobank.itv.mo_datauser.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserDAO {
    private static Logger log = LoggerFactory.getLogger(UserDAO.class);
	private SqlSessionFactory sqlSessionFactory = null;

	public UserDAO(SqlSessionFactory sqlSessionFactory){
        this.sqlSessionFactory = sqlSessionFactory;
    }

    public int updateUserGrade(int pgm_key, String day) {
        int count = -1;
        SqlSession session = sqlSessionFactory.openSession();

        try {
            
            Map<String, Object> paramMap = new HashMap<String, Object>();
            paramMap.put("pgm_key", pgm_key);
            
            count = session.update("User.updateUserGrade", paramMap);
            session.update("User.resetUserDay", paramMap);
            
            if("01".equals(day)) {
            	log.info("User.resetUserMonth : "  + day);
            	session.update("User.resetUserMonth", paramMap);	
            }
            
            
            session.commit();
        } catch(Exception e)  {
            log.error("User.updateUserGrade : " , e);
            session.rollback();
        } finally {
            session.close();
        }

        return count;
    }
}
