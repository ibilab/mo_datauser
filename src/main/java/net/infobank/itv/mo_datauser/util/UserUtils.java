package net.infobank.itv.mo_datauser.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datauser.dao.UserDAO;

public class UserUtils {
    private static Logger log = LoggerFactory.getLogger(UserUtils.class);
    
    public static int updateUserGrade(int pgm_key, String day) {
        UserDAO userDAO = new UserDAO(MyBatisConnectionFactory.getSqlSessionFactory());
        int count = 0;
        try {
        	
  
        	
            count = userDAO.updateUserGrade(pgm_key, day);
        } catch (Exception e) {
            log.error("Exception updateUserGrade : ", e);
        }
        return count;
    }
}
