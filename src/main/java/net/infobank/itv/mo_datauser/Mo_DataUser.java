package net.infobank.itv.mo_datauser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import net.infobank.itv.mo_common.model.Schedule;
import net.infobank.itv.mo_common.util.FileConfig;
import net.infobank.itv.mo_common.util.MyBatisConnectionFactory;
import net.infobank.itv.mo_datauser.util.ScheduleUtils;
import net.infobank.itv.mo_datauser.util.UserUtils;

public class Mo_DataUser {
    private static Logger log = LoggerFactory.getLogger(Mo_DataUser.class);

    public static void main(String[] args) {
        Mo_DataUser data_proc = new Mo_DataUser();
        data_proc.start(args);
    }
    
    public void start(String[] args) {
        FileConfig _config;

        // 접속정보 를 파일에서 가져온다.
        if (args.length != 1) {
            System.err.println("error: not enough argument");
            System.err.println("<arg1:config file>");
            return;
        }
        
        _config = new FileConfig(args[0]);
        
        String group_name = new Object() {}.getClass().getEnclosingClass().getSimpleName();
        MDC.put("log_file", _config.getLog_dir() + "/" + group_name+ "/" + group_name);
        
        Properties props = new Properties();
        props.put("driver", _config.getDriver());
        props.put("url", _config.getUrl());
        props.put("user", _config.getUser());
        props.put("password", _config.getPw());
        MyBatisConnectionFactory.setSqlSessionFactory(props);
        
        String version = _config.getVersion(); // 버전 
        int nVersion = Integer.parseInt(version); 
        
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        Date date = new Date();
        String stat_datetime = sdf.format(date);
        
        log.info("Today is " + stat_datetime);
    	
        
        // pgm_key 가져오기
        if(nVersion == 2) { 
	        ArrayList<Schedule> schedule_list = (ArrayList<Schedule>) ScheduleUtils.getMOSchedule();
	        try {
	            for(int i=0 ; i<schedule_list.size() ; i++) {
	                Schedule s = schedule_list.get(i);
	                int pgm_key = s.getPgm_key();
	
	                UserUtils.updateUserGrade(pgm_key, stat_datetime);
	            }
	        } catch (Exception e) {
	            log.error("Exception", e);
	        }       
        }
        else {
	        ArrayList<Schedule> schedule_list = (ArrayList<Schedule>) ScheduleUtils.getMOSchedule();
	        try {
	            for(int i=0 ; i<schedule_list.size() ; i++) {
	                Schedule s = schedule_list.get(i);
	                int pgm_key = s.getPgm_key();
	
	                UserUtils.updateUserGrade(pgm_key, stat_datetime);
	            }
	        } catch (Exception e) {
	            log.error("Exception", e);
	        }    
        }
    }

}
